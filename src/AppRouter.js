import React from "react";
import HomePage from "./components/Homepage";
import EnhancedTable from "./components/Insight's";
import APIDoc from "./components/Api";
import Similar from "./components/SimilarWebsite";

import { BrowserRouter as Router, Route } from "react-router-dom";

export const AppRouter = () => {
  return (
    <Router>
      <div>
        <Route exact path="/" component={HomePage} />
      </div>
      {/* <div>
        <Route exact path="/insight" component={EnhancedTable} />
      </div>
      <div>
        <Route exact path="/api" component={(APIDoc)} />
      </div> */}
      <div>
        <Route exact path="/similar" component={Similar} />
      </div>
    </Router>
  );
};

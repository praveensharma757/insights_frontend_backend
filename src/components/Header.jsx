import PropTypes from "prop-types";
import React, { Component } from "react";
import { URL } from "./utils";
import "./bootstrap/css/bootstrap.min.css";
import { Dropdown } from "react-bootstrap";

export default class Header extends Component {
  static propTypes = {
    authenticated: PropTypes.bool.isRequired,
  };

  render() {
    const { user, data, authenticated } = this.props;

    return (
      <>
        <nav class="navbar navbar-light navbar-expand-md bg-faded justify-content-center">
          <div class="navbar-collapse collapse w-100">
            <li>
              <img src="toolmark.png" alt="logo" className="toolmark"></img>
            </li>
            <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
              <li class="nav-item">
                <Dropdown>
                  {authenticated ? (
                    <Dropdown.Toggle variant="string" id="dropdown-basic">
                      Hello, {user.name}!
                    </Dropdown.Toggle>
                  ) : (
                    <Dropdown.Toggle variant="string" id="dropdown-basic">
                      Hello, Guest!
                    </Dropdown.Toggle>
                  )}
                  <Dropdown.Menu>
                    {authenticated ? (
                      <>
                        {/* <Dropdown.Item
                          id="dropdown-item"
                          onClick={this._handleInsightClick}
                        >
                          Insight's
                        </Dropdown.Item> */}
                        {/* <Dropdown.Item
                          id="dropdown-item"
                          onClick={this._handleAPIClick}
                        >
                          API
                        </Dropdown.Item> */}
                        <Dropdown.Item
                          id="dropdown-item"
                          onClick={this._handleLogoutClick}
                        >
                          Logout
                        </Dropdown.Item>
                      </>
                    ) : (
                      <Dropdown.Item onClick={this._handleSignInClick}>
                        Login
                      </Dropdown.Item>
                    )}
                  </Dropdown.Menu>
                </Dropdown>
              </li>
            </ul>
          </div>
        </nav>
      </>
    );
  }
  _handleSignInClick = () => {
    // Authenticate using via passport api in the backend
    // Open Twitter login page
    window.open(URL + `/auth/google`, "_self");
  };

  _handleLogoutClick = () => {
    // Logout using Twitter passport api
    // Set authenticated state to false in the HomePage component
    window.open(URL + `/logout`, "_self");
  };

  // _handleAPIClick = () => {
  //   window.open(`/api`, "_self");
  // };

  // _handleInsightClick = () => {
  //   window.open(`/insight`, "_self");
  // };
}

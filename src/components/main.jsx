import React, { Component } from "react";
import Bookmark from "./Bookmark";

export default class Main extends Component {
  render() {
    const { user, data } = this.props;

    return (
      <div>
        <div>
          <div className="tabbed">
            <input type="radio" name="tabs" id="tab-nav-1" defaultChecked />
            <label htmlFor="tab-nav-1">Search</label>

            <div className="tabs">
              <div className="bookmark_hashtag">
                <div className="bookmark">
                  <Bookmark data={data} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import Similar from "./SimilarWebsite";

export default class BookmarkDetails extends Component {
  updateHashtags = (e) => {
    const hashtag = e.target.value;
    console.log(hashtag);
    e.obj = {
      url: this.props.url,
      hashtag: hashtag,
    };
    fetch(`http://localhost:3000/hashtag`, {
      method: "POST",
      credentials: "include",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(e.obj),
    });
    console.log(e);
  };
  getHashtag = (e) => {
    let changeHashtag = document.getElementById("changeHashtag").value;
    console.log(changeHashtag);
    this.setState({ changeHashtag });
  };

  render() {
    const { url, hashtag } = this.props;
    let newurl = "https://" + url;

    return (
      <div className="row">
        <a href={newurl} className="col-md-4">
          {newurl}
        </a>
        <a className="col-md-12" target="blank">
          {url}
        </a>
        <textarea
          onChange={this.getHashtag}
          id="changeHashtag"
          className="col-md-12"
        >
          {hashtag}
        </textarea>
        <button
          onClick={this.updateHashtags}
          value={this.getHashtag.changeHashtag}
        >
          Update
        </button>
        <br /> <br />
      </div>
    );
  }
  _handleISimilar = () => {
    window.open(`/similar`, "_self");
  };
}

import { Dropdown, Toast } from "react-bootstrap";
import React, { Component } from "react";

export default class DownloadExtension extends Component {
  render() {
    return (
      <>
        <div className="downloadextension">
          <p>
            {" "}
            Don't forget to Download the extension,{" "}
            <a
              target="_self"
              href="https://chrome.google.com/webstore/detail/toolmark-a-bookmark-manag/iaccedgdmhegoagdfmlpafaehmokckdn"
              rel="noopener noreferrer"
            >
              click here!
            </a>
          </p>
        </div>
      </>
    );
  }
}

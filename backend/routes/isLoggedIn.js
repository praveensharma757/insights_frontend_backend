const users = require("../model/userModel");
const bookmarks = require("../model/userBookmark");

module.exports = (req, res) => {
  try {

    if(req.user){
        bookmarks
        .findOne({
          email: req.user.email,
        })
        .then((currentUser) => {
          if (currentUser !== null) {
              // console.log(JSON.parse(currentUser.data))
            return res.json({
                message: "Ok",
                user: true,
                data: JSON.parse(currentUser.data),
              });
          }
        });
    }else{
        return res.json({
            message: "Failed",
            user: false,
          });
    }
  } catch (e) {
    console.log(
      "Something Went wrong May be you are not login please login first..." + e
    );
    return res.json({
      message: "Failed",
      user: false,
    });
  }
 
};
const users = require("../model/userModel");
const bookmarks = require("../model/userBookmark");

module.exports = (req, res) => {
  console.log(req.body);
  let entries = [];
  let prevEntries = [];
  let objarry = {};
  let body = req.body;
  let data = body;

  for (var key in data) {
    if (data.hasOwnProperty(key) && data[key].liked) {
      entries.push(data[key]);
    }
  }

  const convertArrayToObject = (array, key) => {
    const initialValue = {};
    return array.reduce((obj, item) => {
      return {
        ...obj,
        [item[key]]: item,
      };
    }, initialValue);
  };

  try {
    bookmarks
      .findOne({
        email: req.user.email,
      })
      .then((currentUser) => {
        if (currentUser !== null) {
          let obj = JSON.parse(currentUser.data);
          Object.entries(obj).forEach((entry) => {
            prevEntries.push(entry[1]);
          });
        }

        if (body.user !== req.user.email) {
          prevEntries = prevEntries.concat(entries);
          objarry = convertArrayToObject(prevEntries, "url");
        } else {
          objarry = convertArrayToObject(entries, "url");
        }
        // console.log(objarry)

        users
          .findOne({
            email: req.user.email,
          })
          .then((user) => {
            if (user) {
              bookmarks.findOneAndUpdate(
                {
                  email: user.email,
                },
                {
                  $set: {
                    email: req.user.email,
                    data: JSON.stringify(objarry),
                  },
                },
                {
                  new: true,
                  upsert: true,
                },
                function (err, doc) {
                  if (err) {
                    console.log("Something wrong when updating data!");
                  }
                  console.log("Data refreshed in DB");
                  // console.log(objarry)
                  return res.json({
                    message: "Ok",
                    user: req.user.email,
                    data: objarry,
                    entries: prevEntries,
                  });
                }
              );
            } else {
              console.log("please login first...");
            }
          });
      });
  } catch (e) {
    console.log(
      "Something Went wrong May be you are not login please login first..." + e
    );
    return res.json({
      message: "Failed",
      user: "notLoggedIn",
    });
  }
};

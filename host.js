const express = require("express");
const path = require("path");
const passport = require("passport");
const mongoose = require("mongoose");
const users = require("./backend/model/userModel");
const cookieSession = require("cookie-session");
const profileRoute = require("./backend/routes/profileRoute");
const passportSetup = require("./backend/config/passport-setup");
const dbUpdate = require("./backend/routes/dbRoute");
const similarWebsite = require("./backend/routes/similarWebsite");
const updateClusterData = require("./backend/routes/updateClusterData");
const isLoggedIn = require("./backend/routes/isLoggedIn");
const getClusterData = require("./backend/routes/getClusterData");
const keys = require("./backend/config/keys");
const dotenv = require("dotenv").config();
const bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(
  cookieSession({
    maxAge: 24 * 60 * 60 * 1000,
    keys: ["newCookieSessionKey"],
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, "build")));

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  users.findById(id).then((user) => {
    done(null, user);
  });
});

app.use("/login/success", profileRoute);
app.get("/auth/google", (req, res, next) => {
  if (req.user) {
    //console.log("Inside /auth/google ");
    return res.redirect(`/`);
  } else {
    return res.redirect("/login");
  }
});

app.get(
  "/login",
  passport.authenticate("google", {
    scope: ["profile", "email"],
  })
);

app.get("/logout", async (req, res) => {
  await req.logout();
  console.log("logout from server");
  req.session = null;
  res.clearCookie("express:sess");
  res.clearCookie("express:sess.sig");
  return res.redirect(`/`);
});

app.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: `/`,
    failureRedirect: "/auth/login/failed",
  })
);

app.get("/login/failed", (req, res) => {
  return res.status(401).json({
    success: false,
    message: "user failed to authenticate.",
  });
});

app.post("/userInfoUpdate", dbUpdate);
app.get("/similarWebsite", similarWebsite);
app.get("/updateClusterData", updateClusterData);
app.get("/getClusterData", getClusterData);
app.get("/isLoggedIn", isLoggedIn);

app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

const PORT = process.env.PORT || 80;
mongoose
  .connect(keys.mongoDb, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => console.log("Database Connected"))
  .catch((error) => console.log(error));

app.listen(PORT, () => {
  console.log("Node Backend Server running on port*: " + PORT);
});
